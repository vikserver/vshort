"use-strict";

var ready=new Promise(r=>self.addEventListener("load", r));

async function main(){
    let uid=location.hash.replace("#","");
    if(uid == ""){
        location.href="https://viksrv.tk";
    } else {
        let response=await fetch(`https://vikserver-api.herokuapp.com/get-link/${uid}`).then(a=>a.json());
        if(response.Error){
            throw new Error(response.Error);
        }
        location.href=response.Short;
    }
}

main().catch(async e=>{
    await ready;
    document.querySelector("[data-titulo]").innerText=e;
    document.querySelector(".spin").style.visibility="hidden";
});
