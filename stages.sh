#!/bin/bash

copy_contents(){
    stat $1 &>/dev/null&&rm -rf $1
    mkdir $1
    cp -r --target-directory=$1 $(cat files)||exit 1
}

make_tarball(){
    t_name=$1
    temp_name="$t_name.temp"
    copy_contents $temp_name
    stat $t_name &>/dev/null&&rm -rf $t_name
    tar --exclude=.* --exclude=*.sh --exclude=node_modules \
        -C $temp_name -cf $t_name $(cat files) node_modules||exit 50
    stat $t_name >/dev/null&&echo "Tarball created at $t_name"

    rm -rf $temp_name
}

extract_tarball(){
    t_name=$1
    out=$2
    stat $out&>/dev/null&&rm -rf $out
    mkdir $2
    tar -C $out -xf $t_name ||exit 100
    stat "$out/index.html" >/dev/null&&echo "Extracted on $out correctly"
}

if [[ "$1" == "build_netlify" ]]; then
    stat public &>/dev/null&&rm -rf public
    copy_contents public||exit 50
fi
